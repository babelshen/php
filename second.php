<?php

include 'header.html';

interface Drivable {
    public function drive();
}

trait GPS {
    public function getCurrentLocation() {
        echo "Searching for the car's location...<br>";

        $latitude = mt_rand(-90, 90) + mt_rand() / mt_getrandmax();
        $longitude = mt_rand(-180, 180) + mt_rand() / mt_getrandmax();

        return "Latitude: $latitude N, Longitude: $longitude W<br>";
    }
}

class Vehicle {
    public $brand;
    private $model;

    public function __construct($brand, $model) {
        $this->brand = $brand;
        $this->model = $model;
    }

    public static function brake() {
        return "The car is braking.";
    }

    public function getInfo() {
        return "Brand: $this->brand, Model: $this->model";
    }

    public function __debugInfo() {
        $properties = get_object_vars($this);
        return $properties;
    }
}

class Car extends Vehicle implements Drivable {
    
    use GPS;
    public $color;

    public function __construct($brand, $model, $color) {
        parent::__construct($brand, $model);
        $this->color = $color;
    }

    public function drive() {
        return "The car is driving.";
    }

    public function getInfo() {
        $parentInfo = parent::getInfo();
        echo "$parentInfo, Color: $this->color";
    }

    public function checkSpeedLimit($speed) {
        $speedLimit = 120;
        if ($speed > $speedLimit) {
            return "You are exceeding the speed limit!";
        } else {
            return "You are driving within the speed limit.";
        }
    }

    public static function turnOnHeadlights() {
        return "Headlights are turned on.";
    }

    public function accelerate($speed) {
        echo self::checkSpeedLimit($speed) . "<br>";
        return "The car is accelerating to $speed km/h.";
    }
}


$car = new Car("Tesla", "Model S", "Red");

echo $car -> getInfo() . "<br>";
echo $car -> getCurrentLocation() . "<br>";
echo $car -> checkSpeedLimit(200) . "<br>";
echo $car -> turnOnHeadlights() . "<br>";
echo $car -> accelerate(100) . "<br>";
echo $car -> accelerate(150) . "<br>";
echo $car -> brake() . "<br>";
echo $car -> drive() . "<br>";
var_dump($car);

echo '<br><br>////////////////////// <br><br>';

class Singleton
{
    private $props = [];
    private static $instance;

    protected function __construct(){}

    protected function __clone(){}

    public function __wakeup() 
    {
        throw new Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): Singleton
    {
        if (empty(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function getProperty(string $key)
    {
        return $this -> props[$key];
    }

    public function setProperty(string $key, string $value)
    {
        return $this -> props[$key] = $value;
    }
}

$config = Singleton::getInstance();
$config -> setProperty("key1", "value1");
unset($config);
$config1 = Singleton::getInstance();
echo $config1 -> getProperty("key1");

?>
