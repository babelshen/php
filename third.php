<?php 

include('header.html');

?>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method='POST'>
    <label for="username">Username:</label>
    <input type='text' name='username' />
    <br>
    <label for="password">Password:</label>
    <input type='password' name='password' />
    <br><br>
    <input type='submit' name='login' value='Login' />
</form>

<?php

$users = array(
    "Admin" => "12345qwaqwa",
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_REQUEST['username'])) echo 'Type your username' . "<br>";
    elseif (empty($_REQUEST['password'])) echo 'Type your password' . "<br>";
    else {

        $userName = filter_input(
            INPUT_POST,
            'username', 
            FILTER_SANITIZE_SPECIAL_CHARS 
        );

        $password = filter_input(
            INPUT_POST,
            'password', 
            FILTER_SANITIZE_SPECIAL_CHARS 
        );

        function login($users, $userName, $password) {
            foreach ($users as $name => $dbpassword) {
                if ($name === $userName && $dbpassword === $password) {
                    return true;
                }
            }
            return false;
        }
        
        $login = login($users, $userName, $password);
        if ($login) {
            echo"Welcome, $userName!" . "<br>";
            echo"Go to <a href='extra.php' target='_blank'>Extra Page</a>";
            $_SESSION["username"] = $_REQUEST['username'];
            $_SESSION["password"] = $_REQUEST['password'];
        }
        else echo"You type incorrect account information" . "<br>";
    }
}

?>

<form action='third.php' method='GET'>
    <label for="search_param">Country:</label>
    <input type='text' name='search_param' />
    <input type='submit' name='search' value='Search' />
</form>

<?php 

$db = array(
    "Ukraine" => "Kyiv",
    "Poland" => "Warsaw",
    "Czech Republic" => "Prague",
    "France" => "Paris",
    "UK" => "London",
    "Portugal" => "Lisbon",
    "Germany" => "Berlin",
    "Italy" => "Rome",
);

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    if (empty($_REQUEST['search_param'])) echo 'Type country' . "<br>";
    else {
        $country = filter_input(
            INPUT_GET,
            'search_param', 
            FILTER_SANITIZE_SPECIAL_CHARS 
        );
        function searchCapital($db, $searchCountry) {
            foreach ($db as $country => $capital) {
                if ($country === $searchCountry) {
                    return $capital;
                }
            }
            return null;
        }

        $search = searchCapital($db, $country);
        if ($search !== null) {
            echo "The capital of '$country' is: $search <br>";
            setcookie("best_trip", $country, time() + 86400 * 2, "/");
        } else {
            echo "The capital of $country not found<br>";
        }
    }
}

echo "<br><br>";
if (isset($_COOKIE["best_trip"])) {
    echo"Go to " . $_COOKIE["best_trip"] . " TODAY!!!! Just call to us and get details)<br><br>";
}

class RequestWrapper {
    public static function get($key) {
        return isset($_GET[$key]) ? $_GET[$key] : null;
    }

    public static function post($key) {
        return isset($_POST[$key]) ? $_POST[$key] : null;
    }

    public static function hasGet($key) {
        return isset($_GET[$key]);
    }

    public static function hasPost($key) {
        return isset($_POST[$key]);
    }

    public static function getAllGet() {
        return $_GET;
    }

    public static function getAllPost() {
        return $_POST;
    }

    public static function getAllAsString() {
        return http_build_query($_REQUEST);
    }
    public static function countGetMethods() {
        return count($_GET);
    }
    public static function countPostMethods() {
        return count($_POST);
    }
}


echo var_export(RequestWrapper::hasGet('search'), true) . "<br>";
echo var_export(RequestWrapper::getAllPost(), true) . "<br>";
echo var_export(RequestWrapper::getAllAsString(), true) . "<br>";
echo var_export(RequestWrapper::countPostMethods(), true) . "<br>";
echo var_export(RequestWrapper::countGetMethods(), true) . "<br>";

?>