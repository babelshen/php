<?php

include ('header.html');

// Comment

/*
    One
    More
    comment
*/

$name = "Illia";
$age = 28;
$city = "Kyiv";
$hello = 'Hello, ';
$world = 'world!';

$result = "Hello, my name is $name. I am $age years old and I live in $city.<br>";
$badResult = 'Hello, my name is $name. I am $age years old and I live in $city.<br>';

echo $result . "<br>";
echo $badResult . "<br>";

echo $hello . $world . "<br>";

$hello .= $world . "<br><br>";

echo $hello;
?>

<form action='first.php' method='POST'>
    <label for="string">String:</label>
    <input type='text' name='string' />
    <br><br>
    <input type='submit' name='letter' value='Receive first letter' />
    <input type='submit' name='length' value='Receive length of string' />
    <input type='submit' name='lowercase' value='String in lower case' />
    <input type='submit' name='uppercase' value='String in upper case' />
    <input type='submit' name='reverse' value='Reverse string' />
    <input type='submit' name='pad' value='Pad' />

</form>

<?php 

    if (isset($_POST['string'])) {
        $string = $_POST['string'];

        if (isset($_POST['letter'])) {
            $firstLetter = substr($string, 0, 1);
            echo "First letter of the string: $firstLetter";
        }
        elseif (isset($_POST['length'])) {
            $length = strlen($string);
            echo "Length of the string: $length";
        }
        elseif (isset($_POST['lowercase'])) {
            $lowerCase = strtolower($string);
            echo "Lower case style: $lowerCase";
        }
        elseif (isset($_POST['uppercase'])) {
            $upperCase = strtoupper($string);
            echo "Upper case style: $upperCase";
        }
        elseif (isset($_POST['reverse'])) {
            $reverse = strrev($string);
            echo "Result: $reverse";
        }
        elseif (isset($_POST['pad'])) {
            $pad = str_pad($string, 20, "0");
            echo "Result: $pad";
        }
        else {
            echo"Error!";
        }
    }
?>
<br><br>
<hr>
Numbers
<hr>
<br>
<form action='first.php' method='POST'>
    <label for="x">x:</label>
    <input type='text' name='x' />

    <label for="y">y:</label>
    <input type='text' name='y' />

    <label for="z">z:</label>
    <input type='text' name='z' />
    <br><br>
    <input type='submit' name='sum' value='Sum' />
    <input type='submit' name='subtraction' value ='Subtraction' />
    <input type='submit' name='pow' value ='Pow' />
    <input type='submit' name='max' value ='Max' />
    <input type='submit' name='round' value ='Round' />
    <input type='submit' name='floor' value ='Floor' />
    <input type='submit' name='ceil' value ='Ceil' />
    <input type='submit' name='calc' value='Calculator' /> 
</form>

<?php

isset($_POST['calc']) ? exec('start calc') : false;

if (isset($_POST['sum']) || isset($_POST['subtraction']) || isset($_POST['pow']) || isset($_POST['max'])) {
    $x = filter_input(INPUT_POST, 'x', FILTER_SANITIZE_NUMBER_INT);
    $y = filter_input(INPUT_POST, 'y', FILTER_SANITIZE_NUMBER_INT);

    if (!empty($x) && !empty($y)) {

        if (isset($_POST['sum'])) echo $x + $y . "<br>";
        elseif (isset($_POST['subtraction'])) echo $x - $y . "<br>";
        elseif (isset($_POST['pow'])) echo pow($x, $y) . "<br>";
        elseif (isset($_POST['max'])) echo max($x, $y) . "<br>";
    }
    else {
        echo "You must enter integers!<br>";
    }    

    echo "Check on your calculator by clicking on the \"Calculator\" button" . "<br>";
}

if (isset($_POST['round']) || isset($_POST['floor']) || isset($_POST['ceil'])) {
    $z = filter_input(INPUT_POST, 'z', FILTER_VALIDATE_FLOAT);
    if (!empty($z)) {
        if (isset($_POST['round'])) echo round($z) . "<br>";
        elseif (isset($_POST['floor'])) echo floor($z) . "<br>";
        elseif (isset($_POST['ceil'])) echo ceil($z) . "<br>";
    }

    else {
        echo "You need to enter a float in the z field<br>";
    }    
}

?>

<br><br>
<hr>
Array
<hr>
<br>

<form action='first.php' method='POST'>
    <label for="fruits">Type your favorite fruits:</label>
    <input type='text' name='fruits' />
    <label for="param">Type additional param:</label>
    <input type='text' name='param' />
    <br><br>
    <input type='submit' name='create_arr' value='Create List' />
    <input type='submit' name='second' value='Receive second elem' />
    <input type='submit' name='length' value='List length' />
    <input type='submit' name='del_first' value='Delete first' />
    <input type='submit' name='del_last' value ='Delete last' />
    <input type='submit' name='reverse' value='Reverse view' />
    <input type='submit' name='unique' value='Unique' />
    <input type='submit' name='check' value='Check' />
    <input type='submit' name='search' value='Search' />
    <input type='submit' name='sort' value='Sort' />
    <input type='submit' name='map' value='Map' />
</form>

<?php

if (isset($_POST['create_arr']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    print_r($result);
    echo "<br>";
}
elseif (isset($_POST['second']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    $result = $result[1];
    echo "Result:" . $result . "<br>";
}
elseif (isset($_POST['length']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $array = explode(' ', $string);
    $result = count($array);
    echo "Result:" . $result . "<br>";
}
elseif (isset($_POST['del_last']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    $delete = array_pop($result);
    echo "We delete:" . $delete . "<br>";
    print_r($result);
    echo "<br>";
}
elseif (isset($_POST['del_first']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    $delete = array_shift($result);
    echo "We delete:" . $delete . "<br>";
    print_r($result);
    echo "<br>";
}
elseif (isset($_POST['reverse']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    $result = array_reverse($result);
    print_r($result);
    echo "<br>";
}
elseif (isset($_POST['unique']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $result = explode(' ', $string);
    $result = array_unique($result);
    print_r($result);
    echo "<br>";
}
elseif (isset($_POST['check']) && isset($_POST['fruits']) && isset($_POST['param'])) {
    $string = $_POST['fruits'];
    $param = $_POST['param'];
    $array = explode(' ', $string);
    $result = in_array($param, $array, true);
    var_dump($result);
    echo "<br>";
}
elseif (isset($_POST['search']) && isset($_POST['fruits']) && isset($_POST['param'])) {
    $string = $_POST['fruits'];
    $param = $_POST['param'];
    $array = explode(' ', $string);
    $result = array_search($param, $array, true);
    var_dump($result);
    echo "<br>";
}
elseif (isset($_POST['sort']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $array = explode(' ', $string);
    sort($array);
    $result = implode(' ', $array);
    echo "$result<br>";
}
elseif (isset($_POST['map']) && isset($_POST['fruits'])) {
    $string = $_POST['fruits'];
    $array = explode(' ', $string);
    $array = array_map(function($item) { return str_shuffle($item); }, $array);
    $result = implode(' ', $array);
    echo "$result<br>";
}
else echo "Something wrong :c";
?>

<br><br>
<hr>
Associative array
<hr>
<br>

<form action='first.php' method='GET'>

    <label for="search">Type search param:</label>
    <input type='text' name='search' />
    <br><br>
    <input type='submit' name='search_all' value='Search full info' />
    <input type='submit' name='count_workers' value='Count workers' />
    <input type='submit' name='search_positions' value='Show all positions' />
    <input type='submit' name='search_workers'value='Show all workers' />
    <input type='submit' name='position' value='Search position' />
 
</form>

<?php 

$workers = array(
    "Peter" => "QA",
    "Ann" => 'DevOps',
    "Lee" => "Frontend dev",
    "Olivia" => 'Backend dev',                
);

if (isset($_GET['search_all'])) {
    foreach($workers as $key => $value) {
        echo"$key => $value<br>";
    }
}
elseif (isset($_GET['count_workers'])) {
    echo count($workers);
}
elseif (isset($_GET['search_positions'])) {
    print_r(array_values($workers));
}

elseif (isset($_GET['search_workers'])) {
    print_r(array_keys($workers));
}

elseif (isset($_GET['position']) && isset($_GET['search'])) {
    function findWorkerByPosition($workers, $position) {
        foreach ($workers as $name => $workerPosition) {
            if (strtolower($workerPosition) === strtolower($position)) {
                return $name;
            }
        }
        return null;
    }
    
    $searchPosition = $_GET['search'];
    $foundPosition = findWorkerByPosition($workers, $searchPosition);
    
    if ($foundPosition !== null) {
        echo "Workers with position '$searchPosition' found: $foundPosition <br>";
    } else {
        echo "Workers with position '$searchPosition' not found<br>";
    }
} 

?>

<br><br>
<hr>
Unpacking (or destructuring) 
<hr>
<br>

<?php

$person = ['John', 'Smith', 30];
list($firstName, $lastName, $age) = $person;

echo $firstName . "<br>";
echo $lastName . "<br>";
echo $age . "<br><br>";

$arrayNum = [10, 20, 30];
[$a, $b, $c] = $arrayNum;

echo $a . "<br>";
echo $b . "<br>";
echo $c . "<br>";

?>

<br><br>
<hr>
Comparison
<hr>
<br>

<form action='first.php' method='POST'>

    <label for="g">Type first value:</label>
    <input type='text' name='g' />
    <label for="h">Type second value:</label>
    <input type='text' name='h' />
    <br><br>
    <input type='submit' name='start' value='Compare' />

</form>

<?php 

if (isset($_POST['start'])) {
    if (empty($_POST['g'])) echo "Type first value!!!";
    elseif (empty($_POST['h'])) echo "Type second value!!!";
    else {
        $g = $_POST['g'];
        $h = $_POST['h'];
        
        switch(true) {
            case $g == $h:
                echo "Values are equal (non-strict comparison)<br>";
            case $g === $h:
                echo "Values are equal (strict comparison)<br>";
                break;
            case $g <> $h:
                echo "Values are not equal<br>";
            case $g < $h:
                echo "The first value is less than the second value<br>";
                break;
            case $g > $h:
                echo "The first value is greater than the second<br>";
                break;
            default:
                echo "Something wrong :c";
        }
    }
}

?>

<br><br>
<hr>
Type casting | Type conversion
<hr>
<br>

<?php
echo "Int<br>";

$k = "123";
var_dump($k);
echo " | ";
var_dump(is_string($k));

echo "<br>";

$intK = (int)$k;
var_dump($intK);
echo " | ";
var_dump(is_int($intK));

echo "<br>";

$intK = intval($k);
var_dump($intK);
echo " | ";
var_dump(is_int($intK));

echo "<br>";

$res = "5" + 5;
var_dump($res);
echo " | ";
var_dump(is_int($res));

echo "<br><br>";
echo "Float<br>";

$k = "3.14";
var_dump($k);
echo " | ";
var_dump(is_string($k));

echo "<br>";

$floatK = floatval($k);
var_dump($floatK);
echo " | ";
var_dump(is_float($floatK));

echo "<br>";

$floatK = (float)$k;
var_dump($floatK);
echo " | ";
var_dump(is_double($floatK));

echo "<br>";

$floatK = doubleval($k);
var_dump($floatK);
echo " | ";
var_dump(is_float($floatK));

echo "<br><br>";
echo "String<br>";

$k = 123;
var_dump($k);
echo " | ";
var_dump(is_int($k));

echo "<br>";

$stringK = (string)$k;
var_dump($stringK);
echo " | ";
var_dump(is_string($stringK));

echo "<br>";

$stringK = strval($k);
var_dump($stringK);
echo " | ";
var_dump(is_string($stringK));

echo "<br><br>";
echo "Boolean<br>";

$k = "Some text";
var_dump($k);
echo " | ";
var_dump(is_string($k));

echo "<br>";

$boolK = (bool)$k;
var_dump($boolK);
echo " | ";
var_dump(is_bool($boolK));

echo "<br>";

$boolK = boolval($k);
var_dump($boolK);
echo " | ";
var_dump(is_bool($boolK));

echo "<br><br>";
echo "Array<br>";

$k = "1,2,3";
var_dump($k);
echo " | ";
var_dump(is_string($k));

echo "<br>";

$arrayK = explode(",", $k);
var_dump($arrayK);
echo " | ";
var_dump(is_array($arrayK));

echo "<br><br>";
echo "Object<br>";

$k = "stdClass"; 
$objectK = (object)$k;
var_dump($objectK);
echo " | ";
var_dump(is_object($objectK));

?>